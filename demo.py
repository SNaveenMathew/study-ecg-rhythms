import os
import pickle as pkl
import numpy as np
import wfdb
from cottonwood.core.blocks.operations import Constant
from viz import make_ecg_plots


# See the README.md for an explanation of the selected ids and classes
DATA_DIR = "mit-bih-arrhythmia-database-1.0.0"
INCLUDE_IDS = [100, 101, 103, 105, 106, 107, 108, 109, 111, 113]
BEAT_WINDOW = 500  # in milliseconds
F_SAMP = 360  # sampling rate in Hz
# The total amount of data to pull from each recording
N_SECONDS = 600


def main():
    images_dir = os.path.join("reports", "demo_images")
    os.makedirs(images_dir, exist_ok=True)

    # Load the model and remove the parts we don't need for making predictions
    filename = "heartbeat_classifier.pkl"
    with open(filename, "rb") as f:
        classifier = pkl.load(f)

    labels_by_index = classifier.blocks["one_hot"].get_labels()

    classifier.remove("one_hot")
    classifier.remove("logistic_copy")
    classifier.remove("difference")
    classifier.remove("mean_sq_loss")
    classifier.remove("hard_max")

    classifier.add(Constant(None), "input")
    classifier.connect("input", "convolution_0")

    half_window = int(F_SAMP * BEAT_WINDOW / (2 * 1000))  # in samples

    for patient_id in INCLUDE_IDS:
        print(f"Rendering data for patient {patient_id}")
        signal, label_locs, labels = load_waves(patient_id)
        # Find all the centerpoints to evaluate
        i_start_eval_center = half_window
        i_end_eval_center = signal.shape[1] - half_window - 1
        pt_spacing = 1
        eval_pts = np.arange(
            i_start_eval_center,
            i_end_eval_center + 1,
            pt_spacing)

        result_history = []
        for eval_pt in eval_pts:
            i_start_eval = eval_pt - half_window
            i_end_eval = eval_pt + half_window
            signal_snippet = signal[:, i_start_eval: i_end_eval + 1]

            # Manually set the input
            classifier.blocks["input"].result = signal_snippet
            classifier.forward_pass()
            result_history.append(classifier.blocks["logistic"].result.ravel())

        result_history = np.array(result_history).transpose()

        make_ecg_plots(
            signal,
            result_history,
            label_locs,
            labels,
            labels_by_index,
            patient_id,
            F_SAMP,
            half_window,
            pt_spacing,
            images_dir)


def load_waves(patient_id):
    signal = get_signal(patient_id)[:, :N_SECONDS * 360]
    label_locs, labels = get_ann(patient_id)
    return signal, label_locs, labels


def get_signal(pid):
    signal, _ = wfdb.rdsamp(os.path.join(DATA_DIR, str(pid)), channels=[0, 1])
    # Calculate the derivative of the channels and add it in to the data
    derivative_scale = 10
    d_signal = derivative_scale * (signal[2:, :] - signal[:-2, :]) / 2
    all_signals = np.concatenate((signal[1: -1, :], d_signal), axis=1)
    return all_signals.transpose()


def get_ann(pid):
    ann = wfdb.rdann(os.path.join(DATA_DIR, str(pid)), "atr")
    return ann.sample, ann.symbol


if __name__ == "__main__":
    main()
